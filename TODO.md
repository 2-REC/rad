# TODO

## DOCS

- [ ] Make technical documentation
- [ ] Look at docs in "TODO" directory
    - [ ] HEAD_TRACKING.txt<br>
        => Infos related to head tracking (as orientation using gyroscope is not reliable)
    - [ ] links.txt<br>
        => Links... (check all and see what to keep)
- [ ] KINECT_AR.pptx
    - [ ] Update if required
	- [ ] Remove links at end, and move them to "links" file
    - [ ] Could be merged with "KINECT_TRACKING.pptx" (?)
- [ ] KINECT_TRACKING.pptx
    - [ ] Update once implementation choices have been made
    - [ ] Check validity of everything (after tests and evolution)
    - [ ] Replace images taken from Internet where possible<br>
        => To avoid copyright issues (or mention owners when required).
    - [ ] Delete images when done with PPT (?)


## BUG FIXES

### MAIN

- [ ] Kinect problem after 5-6 games (kinect related? - probably memory leak)
- [ ] Freeze when quit application (common with Unity - how to do?)


### PLAYER

- [ ] First game reversed
- [ ] Freezes after a few games (memory leak?)
- [ ] See if Vulkan support is OK, and if have any impact<br>
    => Currently added to the project. Vuforia warns it is experimental/not handled, but doesn't seem to cause problems on device.


## CLEANING

### GENERAL

- [ ] Remove logs/prints
- [ ] Delete unused files


### MAIN

- [ ] State Engine:
    - [ ] game_states.xml : delete unused states (game_over, game_end, ... +related scenes)
    - [ ] Move state engine files to game (move out of generic engine)
    - [ ] Change initialisation stuff (make cleaner - related to state engine cleaning)
        - [ ] Get game data from difficulty/options (time, speed, etc)
- [ ] Delete unused files (especially in state engine)
- [ ] Remove OpenCV DLLs from repository!


### PLAYER

- [x] Make changes for unity-multi handling (as for Main)


## ENHANCEMENTS:

### GENERAL

- [ ] Change grid material (allow non squared geometry)<br>
    => Look at<br>
    https://assetstore.unity.com/packages/2d/textures-materials/yughues-free-grids-nets-materials-13004
- [ ] Bake lighting
    - [ ] And remove dynamic lights
- [ ] Add a waiting room before starting game (to allow player to familiarize before starting the game)
    - [ ] Add a "start" button instead of a timer to start the game
- [ ] Add trail to moving ball (?)
- [ ] Make objects glowing (?))


### MAIN

- [ ] Send game info to Player when finished game (points, time, ...)
- [ ] Send orientation related messages on pubOrient (instead of pubMove)
    - [ ] +check other msgs
- [ ] Add sounds
    - [ ] Collisions
    - [ ] Menus?
- [ ] Pause game when losing connection with player(s)
- [ ] KinectYAW => add bool to send kinect yaw or not (settable in settings)<br>
    => And have a single scene dynamically configured at start.
    - disable GameController.OrientationListener
    - disable Kinect.HeadTracker
    - disable Kinect.BodyManager.UseColorInput
    - disable Kinect.BodyManager.UseDepthInput
    - disable Kinect.BodyManager.UseBodyIndexInput
    - [ ] Merge the 2 scenes
- [ ] Add controls to allow camera control when game is playing
- [ ] Add a connection viewer while in game? (current connections)
- [ ] Kinect height
    - [ ] Get Kinect height from config
        => Or find way to compute it?
    - [ ] Send Kinect height to clients
        => could (should?) have an init message


### PLAYER
- [ ] Find how to manage connection to server
    => Server discovery? Hardcoded IP? ...?
- [ ] messageReceiver: filter messages on subscriber name and dispatch (?)
- [ ] Add sounds
    - [ ] Add AudioListener on camera and AudioSource attached to collisions
    - [ ] From server's messages
    - [ ] Menus, score screen, etc.
- [] Add ingame HUD (time? Points?)


## LATER

### SERVER

- [ ] Use AR Foundation instead of Vuforia + Cardboard:
    - Look at:<br>
        - [ ] AR Foundation + stereo see-through<br>
            https://medium.com/@jon_54445/augmented-reality-for-mobile-vr-headsets-adc6d5ed11fe
            Git:
            https://github.com/jondyne/ARcore_mobileVRHeadset/
        - [ ] AR Foundation migration guide:
            https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@3.0/manual/migration-guide.html
            About AR Foundation 1.0:
            https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@1.0/manual/index.html
            About AR Subsystems 2.1:
            https://docs.unity3d.com/Packages/com.unity.xr.arsubsystems@2.1/manual/index.html
            AR Foundation samples:
            https://github.com/Unity-Technologies/arfoundation-samples


### OTHER

- [ ] Create other viewer app<br>
    => Tablet viewer app (with player body, room, ball): Similar previous project "AR Shooter"
