﻿using UnityEngine;

public abstract class GetRotationAbstract : MonoBehaviour {

    protected float initialYaw;
    protected float yawFromKinect;
//TODO: could (should?) be in derived class
    protected bool hasChanged;


    void Start() {
        hasChanged = false;
        yawFromKinect = .0f;
    }

    void Update() {
        Process();
    }


    public void SetOrientation(Vector3 orientation) {
        yawFromKinect = orientation.y;
        hasChanged = true;
    }


    protected abstract void Process();

}
