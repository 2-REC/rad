﻿using UnityEngine;

delegate void Calibrator();

public class SetInitialRotation : MonoBehaviour {

    public Camera ar_camera;
    public float initDelay = 5.0f;

    float initialYaw;
    float time;
    Calibrator calibrate;


    void Start() {
        initialYaw = .0f;
        time = .0f;
        calibrate = new Calibrator(InitCalibration);

        initialYaw = ar_camera.transform.eulerAngles.y;
        //print("Calibration angle: " + initialYaw);
    }

    void Update() {
        transform.rotation = Quaternion.identity;
        calibrate();
    }


    private void ApplyCalibration() {
        transform.Rotate(.0f, -initialYaw, .0f, Space.World);
    }

    private void InitCalibration() {
        if (time >= initDelay) {
            calibrate = new Calibrator(ApplyCalibration);
            return;
        }

        time += Time.deltaTime;
        initialYaw = ar_camera.transform.eulerAngles.y;
        //print("Calibration angle: " + initialYaw);

        transform.Rotate(.0f, -initialYaw, .0f, Space.World);
    }

}
