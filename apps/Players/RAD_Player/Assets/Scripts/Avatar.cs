﻿using UnityEngine;

public class Avatar : MonoBehaviour {

//TODO: find from script
    public Transform head;
    public Transform handLeft;
    public Transform handRight;


    public virtual void SetAsPlayer(bool active) {
        Camera camera = head.GetComponentInChildren<Camera>();
        camera.gameObject.SetActive(active);

//TODO: do better way
        Transform mesh = head.Find("Mesh");
        mesh.gameObject.SetActive(!active);
    }

    public void SetPosition(Vector3 position) {
        transform.position = position;
    }

    public void SetPositions(Vector3 head, Vector3 handLeft, Vector3 handRight) {
        SetPosition(head);
        this.handLeft.position = handLeft;
        this.handRight.position = handRight;
    }


/*
    public void SetOrientation(float orientation) {
        transform.rotation = Quaternion.Euler(Vector3.up * orientation);
    }
*/

    public virtual void SetHeadOrientation(Vector3 orientation) {
        head.transform.eulerAngles = orientation;
    }

}
