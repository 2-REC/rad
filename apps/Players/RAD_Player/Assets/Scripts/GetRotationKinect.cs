﻿using UnityEngine;

public class GetRotationKinect : GetRotationAbstract {

    protected override void Process() {
        transform.rotation = Quaternion.identity;
        transform.Rotate(.0f, yawFromKinect, .0f, Space.World);
    }

}
