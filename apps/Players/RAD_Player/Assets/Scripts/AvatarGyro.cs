﻿using UnityEngine;

public class AvatarGyro : Avatar {

    GetRotationAbstract getRotation;


    public override void SetAsPlayer(bool active) {
        base.SetAsPlayer(active);

        getRotation = head.GetComponent<GetRotationAbstract>();
        if (getRotation == null) {
            Debug.Log("No GetRotation found!");
//TODO: Exception
            //throw System.Exception("");
            return;
        }
    }

/*
// TODO: consider initial orientation (init * orientation)
    public void SetHeadOrientation(Quaternion orientation) {
        head.rotation = orientation;
    }
*/

    public override void SetHeadOrientation(Vector3 orientation) {
//        head.transform.eulerAngles = orientation;
        getRotation.SetOrientation(orientation);
    }

}
