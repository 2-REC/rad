﻿using UnityEngine;

public class CollisionController : MonoBehaviour {

    public GameObject[] ripples;

    private void Awake() {
        for (int i=0; i<ripples.Length; ++i) {
            ripples[i].SetActive(false);
        }
    }

    public void NotifyHit(int index, Vector3 position) {
        GameObject ripple = ripples[index];
        ripple.transform.position = position;
        ripple.SetActive(true);
    }

}
