﻿using SimpleJSON;

public class MessageReceiverClient : MessageReceiver {

    //protected GameController gameController;
    protected IGameController gameController;


    private void Awake() {
        //gameController = GetComponent<GameController>();
        gameController = GetComponent<IGameController>();
        if (gameController == null) {
            throw new System.Exception("ERROR: Problem finding the 'GameController' component!");
        }
    }

    public override void Receive(SpacebrewClient.SpacebrewMessage message) {

        print("RECEIVED MESSAGE");
        print("clientName: " + message.clientName);
        print("name: " + message.name);
        print("type: " + message.type);
        print("value: " + message.value);

        if (message.type == "string") {
            ProcessMessage(message.value);
        }
    }


    //TODO: return bool if handled, or empty message?
    protected virtual JSONNode ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["START"] != null) {
            print("START MESSAGE: " + msg["START"]);
            gameController.NotifyStart(message);
        }
        else if (msg["STOP"] != null) {
            print("STOP MESSAGE: " + msg["STOP"]);
            gameController.NotifyStop(message);
        }
        else if (msg["WAIT"] != null) {
            print("WAIT MESSAGE: " + msg["WAIT"]);
            gameController.NotifyWait(message);
        }
        else if (msg["READY"] != null) {
            print("READY MESSAGE: " + msg["READY"]);
            gameController.NotifyReady(message);
        }
        return msg;
    }

}
