﻿/*
using UnityEngine;
using SimpleJSON;

//TODO: use member varialbes for Vector3 instead of instantiating at each iteration

public class MessageReceiverImpl : MessageReceiver {

    GameController gameController;


    void Awake() {
        gameController = GetComponent<GameController>();
    }

    public override void Receive(SpacebrewClient.SpacebrewMessage message) {
        if (message.type == "string") {
            ProcessMessage(message.value);
        }
    }


    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            if (msg["1"]["position"] != null) {
                ProcessPosition(msg["1"]["position"]);
            }

//TODO: should only consider Y angle (["orientation"][1])
            if (msg["1"]["orientation"] != null) {
                ProcessOrientation(msg["1"]["orientation"]);
            }
        }

        if (msg["ball"] != null) {
            ProcessBall(msg["ball"]);
        }
    }

    void ProcessPosition(JSONNode positions) {
        float x = int.Parse(positions[0]["x"].Value) / 100.0f;
        float y = int.Parse(positions[0]["y"].Value) / 100.0f;
        float z = int.Parse(positions[0]["z"].Value) / 100.0f;
        Vector3 head = new Vector3(x, y, z);
//print("head position: " + head);

        x = int.Parse(positions[1]["x"].Value) / 100.0f;
        y = int.Parse(positions[1]["y"].Value) / 100.0f;
        z = int.Parse(positions[1]["z"].Value) / 100.0f;
        Vector3 handLeft = new Vector3(x, y, z);
//print("left hand position: " + handLeft);

        x = int.Parse(positions[2]["x"].Value) / 100.0f;
        y = int.Parse(positions[2]["y"].Value) / 100.0f;
        z = int.Parse(positions[2]["z"].Value) / 100.0f;
        Vector3 handRight = new Vector3(x, y, z);
//print("right hand position: " + handRight);

        gameController.UpdatePlayerPositions(head, handLeft, handRight);
    }

    void ProcessOrientation(JSONNode orientation) {
        float x = int.Parse(orientation["x"].Value) / 100.0f;
        float y = int.Parse(orientation["y"].Value) / 100.0f;
        float z = int.Parse(orientation["z"].Value) / 100.0f;

        gameController.UpdatePlayerOrientation(new Vector3(x, y, z));
//print("new orientation: " + new Vector3(x, y, z));
    }

    void ProcessBall(JSONNode ball) {
        float x = int.Parse(ball["x"].Value) / 100.0f;
        float y = int.Parse(ball["y"].Value) / 100.0f;
        float z = int.Parse(ball["z"].Value) / 100.0f;
        Vector3 position = new Vector3(x, y, z);
//print("head position: " + ball);

        gameController.UpdateBallPosition(position);
    }

}
*/
using UnityEngine;
using SimpleJSON;

//TODO: use member varialbes for Vector3 instead of instantiating at each iteration

public class MessageReceiverImpl : MessageReceiverClient {

    protected override JSONNode ProcessMessage(string message) {
        var msg = base.ProcessMessage(message);

        //TODO: check return value...
        if (msg["1"] != null) {
            if (msg["1"]["position"] != null) {
                ProcessPosition(msg["1"]["position"]);
            }

//TODO: should only consider Y angle (["orientation"][1])
            if (msg["1"]["orientation"] != null) {
                ProcessOrientation(msg["1"]["orientation"]);
            }
        }

        if (msg["ball"] != null) {
            ProcessBall(msg["ball"]);
        }

        //TODO: OK here, in same function?
        if (msg["WALL"] != null) {
            ProcessCollisionWall(msg["WALL"]);
        }
        else if (msg["PAD"] != null) {
            ProcessCollisionPad(msg["PAD"]);
        }

        return msg;
    }

    void ProcessPosition(JSONNode positions) {
        float x = int.Parse(positions[0]["x"].Value) / 100.0f;
        float y = int.Parse(positions[0]["y"].Value) / 100.0f;
        float z = int.Parse(positions[0]["z"].Value) / 100.0f;
        Vector3 head = new Vector3(x, y, z);
//print("head position: " + head);

        x = int.Parse(positions[1]["x"].Value) / 100.0f;
        y = int.Parse(positions[1]["y"].Value) / 100.0f;
        z = int.Parse(positions[1]["z"].Value) / 100.0f;
        Vector3 handLeft = new Vector3(x, y, z);
//print("left hand position: " + handLeft);

        x = int.Parse(positions[2]["x"].Value) / 100.0f;
        y = int.Parse(positions[2]["y"].Value) / 100.0f;
        z = int.Parse(positions[2]["z"].Value) / 100.0f;
        Vector3 handRight = new Vector3(x, y, z);
//print("right hand position: " + handRight);

        ((GameController)gameController).UpdatePlayerPositions(head, handLeft, handRight);
    }

    void ProcessOrientation(JSONNode orientation) {
        float x = int.Parse(orientation["x"].Value) / 100.0f;
        float y = int.Parse(orientation["y"].Value) / 100.0f;
        float z = int.Parse(orientation["z"].Value) / 100.0f;

        ((GameController)gameController).UpdatePlayerOrientation(new Vector3(x, y, z));
//print("new orientation: " + new Vector3(x, y, z));
    }

    void ProcessBall(JSONNode ball) {
        float x = int.Parse(ball["x"].Value) / 100.0f;
        float y = int.Parse(ball["y"].Value) / 100.0f;
        float z = int.Parse(ball["z"].Value) / 100.0f;
        Vector3 position = new Vector3(x, y, z);
        //print("head position: " + ball);

        ((GameController)gameController).UpdateBallPosition(position);
    }


    void ProcessCollisionWall(JSONNode collision) {
        float x = int.Parse(collision["position"]["x"].Value) / 100.0f;
        float y = int.Parse(collision["position"]["y"].Value) / 100.0f;
        float z = int.Parse(collision["position"]["z"].Value) / 100.0f;
        string side = collision["side"].Value;
        Vector3 position = new Vector3(x, y, z);
        //print("head position: " + ball);

        ((GameController)gameController).AddCollisionWall(position, side);
    }

    void ProcessCollisionPad(JSONNode collision) {
        float x = int.Parse(collision["position"]["x"].Value) / 100.0f;
        float y = int.Parse(collision["position"]["y"].Value) / 100.0f;
        float z = int.Parse(collision["position"]["z"].Value) / 100.0f;
        string side = collision["side"].Value;
        Vector3 position = new Vector3(x, y, z);
        //print("head position: " + ball);

        ((GameController)gameController).AddCollisionPad(position, side);
    }

}
