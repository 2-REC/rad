﻿using System.IO;
using UnityEngine;

public class SessionManager : MonoBehaviour, IServerManager {

    public bool useEditorValues = true;
    public bool updateFile = true;

//TODO: set as readonly properties
    public string ConfigFile = "config.xml";
    public string DefaultServerAddress = "127.0.0.1";
    public string DefaultServerPort = "9000";
    public int DefaultPlayerNb = 1;
    public Vector3 DefaultKinectPosition = new Vector3(float.NaN, float.NaN, float.NaN);

    bool loaded = false;
    Configuration configuration;


    void LoadConfig() {
//TODO: TO TEST!
        if (useEditorValues && Application.isEditor) {
            SetDefault();
Debug.Log("using Editor configuration: " + configuration);

            if (updateFile) {
                SetDefault();
                string pathd = Path.Combine(Application.persistentDataPath, ConfigFile);
                Serializer.Serialize(configuration, pathd);
Debug.Log("updated configuration: " + configuration);
            }

            loaded = true;
            return;
        }

        string path = Path.Combine(Application.persistentDataPath, ConfigFile);
        Debug.Log("Config file path: " + path);

        // if file doesn't exist, create one with default values
        if (!File.Exists(path)) {
            Debug.Log("File " + ConfigFile + " doesn't exist: Creating it with default values");
            SetDefault();
            Serializer.Serialize(configuration, path);
Debug.Log("written configuration: " + configuration);

            loaded = true;
            return;
        }

        configuration = Serializer.Deserialize<Configuration>(path);
Debug.Log("read configuration: " + configuration);

        loaded = true;
    }

    void SetDefault() {
        configuration = new Configuration {
            serverAddress = DefaultServerAddress,
            serverPort = DefaultServerPort,
            playerNb = DefaultPlayerNb
        };
        configuration.kinectPosition[0] = DefaultKinectPosition.x;
        configuration.kinectPosition[1] = DefaultKinectPosition.y;
        configuration.kinectPosition[2] = DefaultKinectPosition.z;
    }


    // IServerManager methods implementation
    public string GetServerAddress() {
        if (!loaded) {
            LoadConfig();
        }
        return configuration.serverAddress;
    }

    public void SetServerAddress(string address) {
        //ignored
    }

    public string GetServerPort() {
        if (!loaded) {
            LoadConfig();
        }
        return configuration.serverPort;
    }

    public void SetServerPort(string port) {
        //ignored
    }


    public int GetPlayerNb() {
        if (!loaded) {
            LoadConfig();
        }
        return configuration.playerNb;
    }

    public Vector3 GetKinectPosition() {
        if (!loaded) {
            LoadConfig();
        }
        return new Vector3(configuration.kinectPosition[0], configuration.kinectPosition[1], configuration.kinectPosition[2]);
    }

}
