﻿using UnityEngine;

//public class GameController : MonoBehaviour {
public class GameController : IGameController {

    public Transform kinect;

    public Avatar avatar;
    public Transform ball;
    public CollisionController collisionController;


//    void Awake() {
    new void Start() {
        base.Start();

        //SessionManager sessionManager = GetComponent<SessionManager>();
        SessionManager sessionManager = commController.GetComponent<SessionManager>();
        if (sessionManager == null) {
            Debug.Log("ERROR: Problem finding the 'SessionManager' component!");
//TODO: Exception
            //throw System.Exception("");
            return;
        }

        Vector3 position = sessionManager.GetKinectPosition();
        if (!float.IsNaN(position.x) && !float.IsNaN(position.y) && !float.IsNaN(position.z)) {
            kinect.position = position;
        }

        avatar.SetAsPlayer(true);
    }

    public void UpdatePlayerPosition(Vector3 position) {
        avatar.SetPosition(position);
    }

    public void UpdatePlayerPositions(Vector3 head, Vector3 handLeft, Vector3 handRight) {
        avatar.SetPositions(head, handLeft, handRight);
    }

    public void UpdatePlayerOrientation(Vector3 orientation) {
//        Quaternion rot = new Quaternion();
//        rot.eulerAngles.Set(orientation.x, orientation.y, orientation.z);
//        avatar.SetHeadOrientation(rot);

        avatar.SetHeadOrientation(orientation);
    }

    public void UpdateBallPosition(Vector3 position) {
        ball.position = position;
    }


    //TODO: use bitmasks for different values in messages
    public void AddCollisionWall(Vector3 position, string side) {
        print("wall collision at " + position + ", " + side);
        //TODO: use enum instead of int for index
        switch (side) {
            case "FRONT":
                collisionController.NotifyHit(0, position);
                break;
            case "LEFT":
                collisionController.NotifyHit(1, position);
                break;
            case "RIGHT":
                collisionController.NotifyHit(2, position);
                break;
            case "BOTTOM":
                collisionController.NotifyHit(3, position);
                break;
            case "TOP":
                collisionController.NotifyHit(4, position);
                break;
            default:
                //exception? or just ignore...
                break;
        }
        //- play sound
    }

    public void AddCollisionPad(Vector3 position, string side) {
        print("pad collision at " + position + ", " + side);
        //TODO:
        //- display collision
        // => LATER, for now nothing
        /*
        switch (side) {
            case "LEFT":
                //
                break;
            case "RIGHT":
                //
                break;
            default:
                //exception? or just ignore...
                break;
        }
        */
        //- play sound
    }

}
