﻿using UnityEngine;
using UnityEngine.SceneManagement;

//TODO: abstract?
//public abstract class IGameController : MonoBehaviour {
public class IGameController : MonoBehaviour {

    public string sceneLobby;
    public string sceneReady;
    public string sceneResult;
    //TODO: change that!
    public string sceneGame1;
    public string sceneGame2;


    protected CommController commController;
    private string currentScene;


    private void Awake() {
        currentScene = SceneManager.GetActiveScene().name;
    }

    //TODO: can be in Awake if force script order (CommController before GameController)
    protected void Start() {
        // Find CommController
        GameObject[] commControllers = GameObject.FindGameObjectsWithTag("CommController");
        if (commControllers.Length != 1) {
            throw new System.Exception("ERROR: Problem finding the 'CommController' object!");
        }

        commController = commControllers[0].GetComponent<CommController>();
        if (commController == null) {
            throw new System.Exception("ERROR: Problem finding the 'CommController' component!");
        }
        print("comm controller ok");

        // Set MessageReceiver for CommController
        SpacebrewEvents spacebrewEvents = commControllers[0].GetComponentInChildren<SpacebrewEvents>();
        if (spacebrewEvents == null) {
            throw new System.Exception("ERROR: Problem finding the 'SpacebrewEvents' component!");
        }
        MessageReceiver messageReceiver = GetComponent<MessageReceiver>();
        spacebrewEvents.messageReceiver = messageReceiver ?? throw new System.Exception("ERROR: Problem finding the 'MessageReceiver' component!");
        print("message receiver ok");

    }


    public void NotifyStart(string message) {
        print("notifySTART:" + message);
        //TODO: start "GAME" scene (1 or 2 depending on number)
        string sceneName = (message == "1") ? sceneGame1 : sceneGame2;
        if (currentScene != sceneName) {
            SceneManager.LoadScene(sceneName);
        }
    }

    public void NotifyStop(string message) {
        print("notifySTOP: " + message);
        //TODO: start "RESULT" scene
        if (currentScene != sceneResult) {
            SceneManager.LoadScene(sceneResult);
        }
    }

    public void NotifyWait(string message) {
        print("notifyWAIT: " + message);
        //TODO: start "LOBBY" scene
        if (currentScene != sceneLobby) {
            SceneManager.LoadScene(sceneLobby);
        }
    }

    public void NotifyReady(string message) {
        print("notifyREADY: " + message);
        //TODO: start "READY" scene
        if (currentScene != sceneReady) {
            SceneManager.LoadScene(sceneReady);
        }
    }



}
