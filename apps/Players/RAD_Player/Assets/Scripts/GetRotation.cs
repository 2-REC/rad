﻿using UnityEngine;

//delegate void Calibrator();

public class GetRotation : GetRotationAbstract {

    public float delay = 5.0f;
    public float convergenceWeight = 50.0f;
    public float maxDelta = 10.0f;

    GameObject obj;

    float time;
    float targetInitialYaw;
//    Calibrator calibrate;


    void Start() {
        time = .0f;
//        calibrate = new Calibrator(InitCalibration);

        InitGyroscope();
        obj = new GameObject();

        transform.rotation = Input.gyro.attitude;
        transform.Rotate(.0f, .0f, 180.0f, Space.Self); // Swap "handedness" of quaternion from gyro
        transform.Rotate(90.0f, 180.0f, .0f, Space.World); // Rotate to point camera out the back of device
        initialYaw = transform.eulerAngles.y;
        //print("Calibration angle: " + initialYaw);

        initialYaw = LimitAngle(initialYaw);
        yawFromKinect = initialYaw;
        targetInitialYaw = initialYaw;
    }

    protected override void Process() {
        // Determining the orientation from the gyroscope & extracting the Y axis component (YAW)
        obj.transform.rotation = Input.gyro.attitude;
        obj.transform.Rotate(.0f, .0f, 180.0f, Space.Self); // Swap "handedness" of quaternion from gyro.
        obj.transform.Rotate(90.0f, 180.0f, .0f, Space.World); // Rotate to make sense as a camera pointing out the back of your device.
        float yRotation = obj.transform.eulerAngles.y;

        Calibrate(yRotation);

        // Applying the Y rotation (YAW) to the object (without the initial yaw angle)
        transform.rotation = Quaternion.identity;
//        transform.Rotate(.0f, yRotation, .0f, Space.World);
        transform.Rotate(.0f, yRotation - initialYaw, .0f, Space.World);
    }


    void InitGyroscope() {
        if (!SystemInfo.supportsGyroscope) {
//TODO: throw exception...
            print("ERROR: NO GYROSCOPE SUPPORT ON DEVICE!");
            Application.Quit();
        }

        Input.gyro.enabled = true;
        Input.gyro.updateInterval = 0.0167f; // highest value (=60 Hz)
    }

    void Calibrate(float yaw) {

        if (initialYaw != targetInitialYaw) {
            Converge();
        }

        time += Time.deltaTime;
        if (time <= delay) {
            return;
        }

        if (hasChanged) {
            hasChanged = false;
            time = .0f;

//            initialYaw = yaw - yawFromKinect;
            targetInitialYaw = LimitAngle(yaw - yawFromKinect);
            Converge();
        }
    }

    void Converge() {
        float delta = LimitAngle(targetInitialYaw - initialYaw);

        delta /= convergenceWeight;
        if (delta > maxDelta) {
            delta = maxDelta;
        }
        else if (delta < -maxDelta) {
            delta = -maxDelta;
        }

        initialYaw += delta;
        initialYaw = LimitAngle(initialYaw);
    }

    float LimitAngle(float angle) {
        float limitedAngle = angle;
        while (limitedAngle <= -180.0f) {
            limitedAngle += 360.0f;
        }
        while (limitedAngle > 180.0f) {
            limitedAngle -= 360.0f;
        }
        return limitedAngle;
    }

}
