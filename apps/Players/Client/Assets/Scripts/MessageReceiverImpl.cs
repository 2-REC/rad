﻿using UnityEngine;
using SimpleJSON;

public class MessageReceiverImpl : MessageReceiver {

    GameController gameController;


    void Awake() {
        gameController = GetComponent<GameController>();
    }

    override public void Receive(SpacebrewClient.SpacebrewMessage message) {
/*
        print("RECEIVED MESSAGE");
        print("clientName: " + message.clientName);
        print("name: " + message.name);
        print("type: " + message.type);
        print("value: " + message.value);
*/

        if (message.type == "string") {
            ProcessMessage(message.value);
        }
    }

    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            if (msg["1"]["position"] != null) {
                float x = int.Parse(msg["1"]["position"][0].Value) / 100.0f;
                float y = int.Parse(msg["1"]["position"][1].Value) / 100.0f;
                float z = int.Parse(msg["1"]["position"][2].Value) / 100.0f;
                gameController.UpdatePlayer1(new Vector3(x, y, z));
//print("new position1: " + new Vector3(x, y, z));
            }
        }
        if (msg["2"] != null) {
            if (msg["2"]["position"] != null) {
                float x = int.Parse(msg["2"]["position"][0].Value) / 100.0f;
                float y = int.Parse(msg["2"]["position"][1].Value) / 100.0f;
                float z = int.Parse(msg["2"]["position"][2].Value) / 100.0f;
                gameController.UpdatePlayer2(new Vector3(x, y, z));
//print("new position2: " + new Vector3(x, y, z));
            }
        }
    }

}
