﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SessionManager))]
public sealed class SessionManagerEditor : Editor {

    SerializedProperty useEditorValuesProp;
    SerializedProperty updateFileProp;

    SerializedProperty configFileProp;

    SerializedProperty defaultServerAddressProp;
    SerializedProperty defaultServerPortProp;
    SerializedProperty defaultPlayerNbProp;
    SerializedProperty defaultKinectPositionProp;


    private void OnEnable() {
        useEditorValuesProp = serializedObject.FindProperty("useEditorValues");
        updateFileProp = serializedObject.FindProperty("updateFile");

        configFileProp = serializedObject.FindProperty("ConfigFile");

        defaultServerAddressProp = serializedObject.FindProperty("DefaultServerAddress");
        defaultServerPortProp = serializedObject.FindProperty("DefaultServerPort");
        defaultPlayerNbProp = serializedObject.FindProperty("DefaultPlayerNb");
        defaultKinectPositionProp = serializedObject.FindProperty("DefaultKinectPosition");
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        EditorGUILayout.PropertyField(useEditorValuesProp, new GUIContent("Use Editor Values"));
        if (useEditorValuesProp.boolValue) {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(updateFileProp, new GUIContent("Update Config File"));
            EditorGUI.indentLevel--;
        }

        EditorGUILayout.PropertyField(configFileProp, new GUIContent("Configuration File"));

        EditorGUILayout.PropertyField(defaultServerAddressProp, new GUIContent("Default Server Address"));
        EditorGUILayout.PropertyField(defaultServerPortProp, new GUIContent("Default Server Port"));
        EditorGUILayout.PropertyField(defaultPlayerNbProp, new GUIContent("Default Player Nb"));
        EditorGUILayout.PropertyField(defaultKinectPositionProp, new GUIContent("Default Kinect Position"));

        serializedObject.ApplyModifiedProperties();
    }

}
