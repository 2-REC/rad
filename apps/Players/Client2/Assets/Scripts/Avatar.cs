﻿using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class Avatar : MonoBehaviour {


//// FULL BODY - BEGIN
//TODO: should move to a separate class, and moved to the "Kinect/Scripts" directory
    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>() {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };
//// FULL BODY - MID



//TODO: find from script
    public Transform head;
    public Material jointMaterial;
    public Material boneMaterial;

    GameObject body;


    void Awake() {
//TODO: find head...

        body = CreateBodyObject();
        
    }

    public void SetAsPlayer(bool active) {
        Camera camera = head.GetComponentInChildren<Camera>();
        camera.gameObject.SetActive(active);

        GetRotation getRotation = head.GetComponent<GetRotation>();
        getRotation.enabled = active;

//TODO: do better way
        Transform mesh = head.Find("Mesh");
        mesh.gameObject.SetActive(!active);
    }


/*
    public void SetPosition(Vector3 position) {
        transform.position = position;
    }
*/
    public void SetPositions(List<Vector3> positions) {
        int nbChildren = body.transform.childCount;
        if (positions.Count != nbChildren) {
            Debug.Log("ERROR: wrong number of children!");
            return;
        }

        for (int i=0; i<nbChildren; ++i) {
            body.transform.GetChild(i).transform.position = positions[i];
        }

        RefreshBodyObject();
    }

    public void SetOrientation(float orientation) {
        transform.rotation = Quaternion.Euler(Vector3.up * orientation);
    }

// TODO: consider initial orientation (init * orientation)
    public void SetHeadOrientation(Quaternion orientation) {
        head.rotation = orientation;
    }


////////
    private GameObject CreateBodyObject() {
        GameObject body = new GameObject("Body");
        body.transform.SetParent(gameObject.transform, false);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++) {
            GameObject jointObj = new GameObject();
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Renderer renderer = sphere.GetComponent<Renderer>();
            if (renderer != null) {
                renderer.material = jointMaterial;
            }
            sphere.transform.localScale = new Vector3(0.06f, 0.06f, 0.06f);
            sphere.transform.parent = jointObj.transform;

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.positionCount = 2;
            lr.material = boneMaterial;
            lr.startWidth = 0.05f;
            lr.endWidth = 0.05f;

            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
////////
            if ((jt == Kinect.JointType.Head) && (head != null)) {
                head.localPosition = Vector3.zero;
                head.SetParent(jointObj.transform, false);
            }
////////
        }

        return body;
    }

    private void RefreshBodyObject() {

        int nbChildren = body.transform.childCount;
        for (int i=0; i<nbChildren; ++i) {
            Transform joint = body.transform.GetChild(i).transform;

            int target = -1;

            if(_BoneMap.TryGetValue((Kinect.JointType)i, out Kinect.JointType jointType)) {
                target = (int)jointType;
            }

            LineRenderer lr = joint.GetComponent<LineRenderer>();
            if (target != -1) {
                Transform targetJoint = body.transform.GetChild(target).transform;
                lr.SetPosition(0, joint.position);
                lr.SetPosition(1, targetJoint.position);
            }
            else {
                lr.enabled = false;
            }
        }
    }
////////

}
