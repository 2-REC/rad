﻿using System.Xml.Serialization;

[XmlRoot("CONFIG")]
public class Configuration {

    [XmlElement("SERVER_ADDRESS")]
    public string serverAddress;

    [XmlElement("SERVER_PORT")]
    public string serverPort;

    [XmlElement("PLAYER_NB")]
    public int playerNb;

    [XmlArray("KINECT_POSITION"), XmlArrayItem("COORDINATE")]
    public float[] kinectPosition = new float[3];


    public override string ToString() {
        string str = "serverAddress: " + serverAddress;
        str += ", serverPort: " + serverPort;
        str += ", playerNb: " + playerNb;
        str += ", kinectPosition: (" + kinectPosition[0] + ", " + kinectPosition[1] + ", " + kinectPosition[2] + ")";
        return str;
    }

}
