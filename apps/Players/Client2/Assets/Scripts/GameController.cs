﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public Transform kinect;

    public Avatar avatar1;
    public Avatar avatar2;


    void Awake() {
        SessionManager sessionManager = GetComponent<SessionManager>();
if (sessionManager == null)
    Debug.Log("NULL");

        int playerNb = sessionManager.GetPlayerNb();
        bool isPlayer1 = true;
//TODO: not perfect...
        if (playerNb == 2) {
            isPlayer1 = false;
        }
Debug.Log("isPlayer1: " + isPlayer1);

        Vector3 position = sessionManager.GetKinectPosition();
        if (!float.IsNaN(position.x) && !float.IsNaN(position.y) && !float.IsNaN(position.z)) {
Debug.Log("have nb");
            kinect.position = position;
        }
Debug.Log("kinect.position: " + kinect.position);


        avatar1.SetAsPlayer(isPlayer1);
        avatar2.SetAsPlayer(!isPlayer1);
    }

    public void UpdatePlayer1(List<Vector3> positions) {
        avatar1.SetPositions(positions);
    }

    public void UpdatePlayer2(List<Vector3> positions) {
        avatar2.SetPositions(positions);
    }

}
