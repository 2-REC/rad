﻿using UnityEngine;
using SimpleJSON;
using System.Collections.Generic;

public class MessageReceiverImpl : MessageReceiver {

    GameController gameController;


    void Awake() {
        gameController = GetComponent<GameController>();
    }

    override public void Receive(SpacebrewClient.SpacebrewMessage message) {
/*
        print("RECEIVED MESSAGE");
        print("clientName: " + message.clientName);
        print("name: " + message.name);
        print("type: " + message.type);
        print("value: " + message.value);
*/

        if (message.type == "string") {
            ProcessMessage(message.value);
        }
    }

/*
    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            if (msg["1"]["position"] != null) {
                float x = int.Parse(msg["1"]["position"][0].Value) / 100.0f;
                float y = int.Parse(msg["1"]["position"][1].Value) / 100.0f;
                float z = int.Parse(msg["1"]["position"][2].Value) / 100.0f;
                gameController.UpdatePlayer1(new Vector3(x, y, z));
//print("new position1: " + new Vector3(x, y, z));
            }
        }
        if (msg["2"] != null) {
            if (msg["2"]["position"] != null) {
                float x = int.Parse(msg["2"]["position"][0].Value) / 100.0f;
                float y = int.Parse(msg["2"]["position"][1].Value) / 100.0f;
                float z = int.Parse(msg["2"]["position"][2].Value) / 100.0f;
                gameController.UpdatePlayer2(new Vector3(x, y, z));
//print("new position2: " + new Vector3(x, y, z));
            }
        }
    }
*/
    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            int index = 0;
            List<Vector3> positions = new List<Vector3>();
            JSONNode msg1 = msg["1"];
            while (msg1[index.ToString()] != null) {
                JSONNode node = msg1[index.ToString()];
                float x = int.Parse(node[0].Value) / 100.0f;
                float y = int.Parse(node[1].Value) / 100.0f;
                float z = int.Parse(node[2].Value) / 100.0f;
                positions.Add(new Vector3(x, y, z));
                ++index;
            }
            gameController.UpdatePlayer1(positions);
        }

        if (msg["2"] != null) {
            int index = 0;
            List<Vector3> positions = new List<Vector3>();
            JSONNode msg2 = msg["2"];
            while (msg2[index.ToString()] != null) {
                JSONNode node = msg2[index.ToString()];
                float x = int.Parse(node[0].Value) / 100.0f;
                float y = int.Parse(node[1].Value) / 100.0f;
                float z = int.Parse(node[2].Value) / 100.0f;
                positions.Add(new Vector3(x, y, z));
                ++index;
            }
            gameController.UpdatePlayer2(positions);
        }
    }

}
