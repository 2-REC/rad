﻿using UnityEngine;

public class Avatar : MonoBehaviour {

//TODO: find from script
    public Transform head;


    public void SetAsPlayer(bool active) {
        Camera camera = head.GetComponentInChildren<Camera>();
        camera.gameObject.SetActive(active);

//TODO: do better way
        Transform mesh = head.Find("Mesh");
        mesh.gameObject.SetActive(!active);
    }

    public void SetPosition(Vector3 position) {
        transform.position = position;
    }

//TODO: Orientation should be applied to the camera
/*
    public void SetOrientation(float orientation) {
        transform.rotation = Quaternion.Euler(Vector3.up * orientation);
    }

// TODO: consider initial orientation (init * orientation)
    public void SetHeadOrientation(Quaternion orientation) {
        head.rotation = orientation;
    }
    public void SetHeadOrientationVect(Vector3 orientation) {
        head.transform.eulerAngles = orientation;
    }
*/

}
