﻿using UnityEngine;
using SimpleJSON;

public class MessageReceiverImpl : MessageReceiver {

    GameController gameController;


    void Awake() {
        gameController = GetComponent<GameController>();
    }

    override public void Receive(SpacebrewClient.SpacebrewMessage message) {
        print("RECEIVED MESSAGE");
        print("clientName: " + message.clientName);
        print("name: " + message.name);
        print("type: " + message.type);
        print("value: " + message.value);

        if (message.type == "string") {
            ProcessMessage(message.value);
        }
    }

    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            if (msg["1"]["position"] != null) {
                float x = int.Parse(msg["1"]["position"][0].Value) / 100.0f;
                float y = int.Parse(msg["1"]["position"][1].Value) / 100.0f;
                float z = int.Parse(msg["1"]["position"][2].Value) / 100.0f;
                gameController.UpdatePlayer(new Vector3(x, y, z));
//print("new position1: " + new Vector3(x, y, z));
            }
        }
    }
/*
    void ProcessMessage(string message) {
        var msg = JSONNode.Parse(message);
        if (msg["1"] != null) {
            if (msg["1"]["orientation"] != null) {
                float x = int.Parse(msg["1"]["orientation"][0].Value) / 100.0f;
                float y = int.Parse(msg["1"]["orientation"][1].Value) / 100.0f;
                float z = int.Parse(msg["1"]["orientation"][2].Value) / 100.0f;
                gameController.UpdatePlayerOrientation(new Vector3(x, y, z));
//print("new orientation: " + new Vector3(x, y, z));
            }
        }

    }
*/

}
