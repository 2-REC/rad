﻿using UnityEngine;

public class GameController : MonoBehaviour {

    public Transform kinect;

    public Avatar avatar;


    void Awake() {
        SessionManager sessionManager = GetComponent<SessionManager>();
        if (sessionManager == null) {
            Debug.Log("No SessionManager found!");
            //throw System.Exception("");
            return;
        }

        Vector3 position = sessionManager.GetKinectPosition();
        if (!float.IsNaN(position.x) && !float.IsNaN(position.y) && !float.IsNaN(position.z)) {
            kinect.position = position;
        }

        avatar.SetAsPlayer(true);
    }

    public void UpdatePlayer(Vector3 position) {
        avatar.SetPosition(position);
    }

/*
    public void UpdatePlayerOrientation(Vector3 orientation) {
//        Quaternion rot = new Quaternion();
//        rot.eulerAngles.Set(orientation.x, orientation.y, orientation.z);
//        avatar.SetHeadOrientation(rot);

        avatar.SetHeadOrientationVect(orientation);
    }
*/

}
