﻿using UnityEngine;

public class ParticleLife : MonoBehaviour {

    ParticleSystem particles;


    private void Start() {
        particles = GetComponent<ParticleSystem>();
    }

    private void Update() {
        if (particles) {
            if (!particles.IsAlive()) {
                //Destroy(gameObject);
                gameObject.SetActive(false);
            }
        }
    }
}
