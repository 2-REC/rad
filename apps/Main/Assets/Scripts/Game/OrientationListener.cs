﻿using SimpleJSON;
using UnityEngine;

public class OrientationListener : MonoBehaviour {

    //public MessageSender messageManager;
    public GameController gameController;
    public float timeInterval = .033f; // 30fps

    float delay;
    bool changed;
    Vector3 orientation;


    private void Awake() {
        //TODO: check controller
        //...

        //TODO: move to Start?
        orientation = Vector3.zero;
    }

    private void Start() {
        changed = false;
        delay = .0f;
    }


/*
    public void NotifyChange(Vector3 orientation) {
        var msg = new JSONClass();

        msg["1"]["orientation"]["x"] = ((int)(orientation.x * 100)).ToString();
        msg["1"]["orientation"]["y"] = ((int)(orientation.y * 100)).ToString();
        msg["1"]["orientation"]["z"] = ((int)(orientation.z * 100)).ToString();

        print("sending message: " + msg.ToString());
        messageManager.SendMove(msg.ToString());
    }
*/

    private void Update() {
        delay += Time.deltaTime;
        if (delay < timeInterval) {
            return;
        }

        if (changed) {
            delay = .0f;
            changed = false;

            var msg = new JSONClass();
            msg["1"]["orientation"]["x"] = ((int)(orientation.x * 100)).ToString();
            msg["1"]["orientation"]["y"] = ((int)(orientation.y * 100)).ToString();
            msg["1"]["orientation"]["z"] = ((int)(orientation.z * 100)).ToString();

            //print("sending message: " + msg.ToString());
            //messageManager.SendMove(msg.ToString());
            gameController.NotifyMove(msg.ToString());
        }
    }


    public void NotifyChange(Vector3 orientation) {
        changed = true;
        this.orientation = orientation;
    }

}
