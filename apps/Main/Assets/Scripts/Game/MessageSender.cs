﻿using UnityEngine;
using SimpleJSON;

//TODO: build all messages here!

public class MessageSender : MonoBehaviour {

    public SpacebrewEvents spacebrewClientEvents;
    public string pubNameGame;
    public string pubNameMove;
    public string pubNameOrient;
    public string pubNameCollision;


    public void SendGame(string message) {
        print("sending game message: " + message);
        spacebrewClientEvents.SendString(pubNameGame, message);
    }

    public void SendMove(string message) {
        spacebrewClientEvents.SendString(pubNameMove, message);
    }

    public void SendOrient(string message) {
        spacebrewClientEvents.SendString(pubNameOrient, message);
    }

    public void SendCollision(string type, string side, Vector3 position) {
        var msg = new JSONClass();
        msg[type]["position"]["x"] = ((int)(position.x * 100)).ToString();
        msg[type]["position"]["y"] = ((int)(position.y * 100)).ToString();
        msg[type]["position"]["z"] = ((int)(position.z * 100)).ToString();
        msg[type]["side"] = side;
print("send: " + msg.ToString());
        spacebrewClientEvents.SendString(pubNameCollision, msg.ToString());
    }

}
