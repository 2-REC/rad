﻿using UnityEngine;

//TODO: use enum for walls
// => even better, use bitmasks & send in message
//TODO : make bounce movements more robust (positive/negative depending on collision plane, instead of just inverting)

//TODO: change this, horrible...
delegate void Processor();

public class BallMovement : MonoBehaviour {

    public GameController gameController;

    //public Vector3 initSpeed = new Vector3(.0f, 1.0f, 1.0f);
    public float initSpeedX = .25f;
    public float initSpeedY = .25f;
    public float initSpeedZ = .5f;
    public float speedFactor = 1.01f;
    public float maxSpeed = 30.0f;

    public CollisionController collisionController;


    Rigidbody body;
    Collider previous;
    Vector3 newVelocity;
    Processor wait;


    private void Start() {
        body = GetComponent<Rigidbody>();
        //body.velocity = initSpeed;
        //body.velocity = new Vector3(initSpeedX, initSpeedY, initSpeedZ);
        body.velocity = Vector3.zero;

        previous = null;

        wait = new Processor(WaitSetup);
    }

    private void Update() {
        wait();
    }


    private void WaitSetup() {
        if (!gameController.levelManager.doingSetup) {
            body.velocity = new Vector3(initSpeedX, initSpeedY, initSpeedZ);
            wait = new Processor(DoNothing);
        }
    }

    private void DoNothing() {
        return;
    }


    private void OnTriggerEnter(Collider other) {

        //print("other: " + other.name);
        if (other == previous) {
            //print("same as last => ignore");
            return;
        }

        Vector3 impact = Vector3.zero;
        Vector3 velocity = body.velocity;
        int index = -1;
        switch (other.name) {
            case "WallRear":
                // HIT!
                impact.Set(velocity.x, velocity.y, Mathf.Abs(velocity.z));
                gameController.NotifyMiss();
                break;
            case "WallFront":
                impact.Set(velocity.x, velocity.y, -Mathf.Abs(velocity.z));
                //TODO: move call to below, just set variables here
                //TODO: use an enum instead of strings & ints
                gameController.NotifyBounce("FRONT", transform.position);
                index = 0;
                break;
            case "WallLeft":
                impact.Set(Mathf.Abs(velocity.x), velocity.y, velocity.z);
                gameController.NotifyBounce("LEFT", transform.position);
                index = 1;
                break;
            case "WallRight":
                impact.Set(-Mathf.Abs(velocity.x), velocity.y, velocity.z);
                gameController.NotifyBounce("RIGHT", transform.position);
                index = 2;
                break;
            case "Floor":
                impact.Set(velocity.x, Mathf.Abs(velocity.y), velocity.z);
                gameController.NotifyBounce("BOTTOM", transform.position);
                index = 3;
                break;
            case "Ceiling":
                impact.Set(velocity.x, -Mathf.Abs(velocity.y), velocity.z);
                gameController.NotifyBounce("TOP", transform.position);
                index = 4;
                break;
            case "HandLeft":
                impact.Set(velocity.x, velocity.y, Mathf.Abs(velocity.z));
                gameController.NotifyHit("LEFT", transform.position);
                break;
            case "HandRight":
                impact.Set(velocity.x, velocity.y, Mathf.Abs(velocity.z));
                gameController.NotifyHit("RIGHT", transform.position);
                break;
            default:
                break;
        }

        if (impact != Vector3.zero) {
            previous = other;

            newVelocity = impact * speedFactor;
            newVelocity = Vector3.ClampMagnitude(newVelocity, maxSpeed);

            //print("new velocity: " + newVelocity);
            body.velocity = newVelocity;

            if (index != -1) {
                collisionController.NotifyHit(index, transform.position);
            }
        }
    }

}
