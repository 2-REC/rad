﻿//using UnityEngine;
//using UnityEngine.UI;

public class GameContinueController : IGameController {

    public Continue cont;
//    public Text textNbContinues;

/*
    new void Start() {
        base.Start();
        textNbContinues.text = "Continues left: " + cont.GetGameData().GetContinues().ToString();
    }
*/

    public void Yes() {
        NotifyWait("YES");
        cont.Yes();
//        textNbContinues.text = "Continues left: " + cont.GetGameData().GetContinues().ToString();
    }

    public void No() {
        NotifyWait("NO");
        cont.No();
    }

}
