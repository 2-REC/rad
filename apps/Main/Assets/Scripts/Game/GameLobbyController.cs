﻿using UnityEngine;
using UnityEngine.UI;

public class GameLobbyController : IGameController {

    //TODO: OK?
    public GameStateController controller;
    public float timeInterval = 1.0f; //?
    public Text textStatus;
    public Button button;

    int nbConnections;
    float delay;


    private void Awake() {
        nbConnections = 0;
        textStatus.text = "WAITING FOR CONNECTION";
        //button.enabled = false;
        button.gameObject.SetActive(false);
    }

    private new void Start() {
        base.Start();

        UpdateConnections(commController.GetNbConnections());
        print("Nb connections: " + nbConnections);
        //print("Level: " + controller.GetGameData().GetLevelName());

        delay = timeInterval;
    }

    private void Update() {
        if (nbConnections > 0) {
            delay += Time.deltaTime;
            if (delay >= timeInterval) {
                //TODO: make a NotifyStop method?
                //TODO: what message?
                //messageSender.SendGame("STOP");
                //NotifyStop("STOP");
                //TODO: what info?
                NotifyReady(controller.GetGameData().GetLevel().ToString());
                delay = .0f;
            }
        }

        int adminNbConnections = commController.GetNbConnections();
        if (adminNbConnections != nbConnections) {
            UpdateConnections(adminNbConnections);
        }

    }


    private void UpdateConnections(int nbConnections) {
        this.nbConnections = nbConnections;
        if (nbConnections == 0) {
            print("NO connections - update text WAITING + disable button...");
            textStatus.text = "WAITING FOR CONNECTION";
            //button.enabled = false;
            button.gameObject.SetActive(false);
        }
        else {
            print("some connections - update text READY + enable button...");
            textStatus.text = "READY";
            //button.enabled = true;
            button.gameObject.SetActive(true);
        }
    }

    public void Go() {
        //TODO: make a NotifyStart method?
        //TODO: what message?
        //messageSender.SendGame("START");
        NotifyStart(controller.GetGameData().GetLevel().ToString());

        // start game scene...
        controller.End();
    }

}
