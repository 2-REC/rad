﻿
public class CommControllerAdmin : CommController {

    SpacebrewAdmin spacebrewAdmin;


    protected override void GetSpaceBrew() {
        base.GetSpaceBrew();

        spacebrewAdmin = GetComponentInChildren<SpacebrewAdmin>();
        if (spacebrewAdmin == null) {
            throw new System.Exception("ERROR: Couldn't find 'SpaceBrewAdmin' component!");
        }
    }

    public int GetNbConnections() {
        return spacebrewAdmin.GetNbConnections();
    }

}
