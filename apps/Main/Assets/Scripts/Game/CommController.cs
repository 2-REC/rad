﻿using UnityEngine;

public class CommController : MonoBehaviour {

    private void Awake() {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("CommController");
        if (objs.Length > 1) {
            gameObject.SetActive(false);
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        GetSpaceBrew();
    }


    protected virtual void GetSpaceBrew() {
        SpacebrewClient spacebrewClient = GetComponentInChildren<SpacebrewClient>(true);
        if (spacebrewClient == null) {
            throw new System.Exception("ERROR: Couldn't find 'SpaceBrewClient' component!");
        }
        spacebrewClient.gameObject.SetActive(true);

    }

}
