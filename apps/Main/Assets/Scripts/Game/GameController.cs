﻿using UnityEngine;

//TODO: build messages in MessageSender!

public class GameController : IGameController {

    public LevelManager levelManager;
    //TODO: move to level stuff? (LvlCtrl?)
    public int damageMiss = 25; //OK here?


    public void NotifyMove(string message) {
        messageSender.SendMove(message);
    }

    public void NotifyBounce(string side, Vector3 position) {
        //TODO:
        // - create collision object
        // !!!! NEED ORIENTATION/NORMAL!

        messageSender.SendCollision("WALL", side, position);
    }

    public void NotifyHit(string side, Vector3 position) {
        //TODO:
        // ?- update game info/UI
        // ?- create collision object
        // !!!! NEED ORIENTATION/NORMAL!

        levelManager.AddPoints(1);
        messageSender.SendCollision("PAD", side, position);
    }

    public void NotifyMiss() {
        //TODO:
        // - update game info/UI
        // ?- create collision object
        // - send message
        //levelManager.NotifyDeath();
        if (levelManager.NotifyHit(damageMiss)) {
            // Dead
            NotifyStop("DEAD"); //+points/time?
        }
    }

    public void NotifyQuit() {
        NotifyStop("QUIT");
        levelManager.Quit();
    }

}
