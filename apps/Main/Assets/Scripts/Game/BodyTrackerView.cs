﻿/*
TODO:
- make generic/dynamic
- add possibility to display other bodies (not only "tracked with heads")
- add associated Editor script for better UI
*/

using UnityEngine;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System;

public class BodyTrackerView : MonoBehaviour {

    public KinectInputManager kinectInputManager;
    public BodyTracker3D bodyTracker3D;


//TODO: make as properties/serializables
//TODO: change boolean to int (more than 2 players)
    public bool singlePlayer;

    public Transform head1;
    public Transform handLeft1;
    public Transform handRight1;

    public Transform head2;
    public Transform handLeft2;
    public Transform handRight2;


    public Material BoneMaterial { get { return boneMaterial; } private set { boneMaterial = value; } }
    [SerializeField]
    private Material boneMaterial;
    public Material JointMaterial { get { return jointMaterial; } private set { jointMaterial = value; } }
    [SerializeField]
    private Material jointMaterial;


    private GameObject body1;
    private GameObject body2;
    public ulong TrackedId1 { get; private set; }
    public ulong TrackedId2 { get; private set; }


    private void Awake() {

//TODO: check that input fields are not null
//...

        TrackedId1 = 0; //OK?
        TrackedId2 = 0; //OK?

        if (!singlePlayer && bodyTracker3D.nbBodiesTracked < 2) {
            throw new Exception("ERROR: Only 1 body tracked!");
        }

        //body1 = CreateBody(head1);
        body1 = CreateBody(head1, handLeft1, handRight1);
        if (!singlePlayer && head2) {
            //body2 = CreateBody(head2);
            body2 = CreateBody(head2, handLeft2, handRight2);
        }
    }

    private void Update () {
//TODO: CHANGE!
// => DOESN'T REMEMBER WHICH PLAYER IS WHICH (if 1 body => always player 1!)
/*
        ulong id = bodyTracker3D.GetTrackedId(1);
        if (id != 0) {
            RefreshBody(id, body1);
        }

        if (!singlePlayer && head2) {
            id = bodyTracker3D.GetTrackedId(2);
            if (id != 0) {
                RefreshBody(id, body2);
            }
        }
*/

        TrackedId1 = bodyTracker3D.GetTrackedId(1);
        if (TrackedId1 != 0) {
            RefreshBody(TrackedId1, body1);
        }

        if (!singlePlayer && head2) {
            TrackedId2 = bodyTracker3D.GetTrackedId(2);
            if (TrackedId2 != 0) {
                RefreshBody(TrackedId2, body2);
            }
        }

    }


    public GameObject GetBody(ulong trackingId) {
        if (trackingId == TrackedId1)
            return body1;
        else if (!singlePlayer && trackingId == TrackedId2)
            return body2;
        else
            return null;
    }


    private GameObject CreateBody(Transform head = null, Transform handLeft = null, Transform handRight = null) {
        GameObject body = new GameObject("Body");
        body.transform.SetParent(gameObject.transform.parent, false);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++) {
            GameObject jointObj = new GameObject();
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            SphereCollider collider = sphere.GetComponent<SphereCollider>();
            if (collider != null) {
                collider.enabled = false;
            }

            Renderer renderer = sphere.GetComponent<Renderer>();
            if (renderer != null) {
                renderer.material = jointMaterial;
            }
            sphere.transform.localScale = new Vector3(0.06f, 0.06f, 0.06f);
            sphere.transform.parent = jointObj.transform;

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.positionCount = 2;
            lr.material = boneMaterial;
            lr.startWidth = 0.05f;
            lr.endWidth = 0.05f;

            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;

            if ((jt == Kinect.JointType.Head) && (head != null)) {
                head.localPosition = Vector3.zero;
                head.SetParent(jointObj.transform);
            }
            else if ((jt == Kinect.JointType.HandLeft) && (handLeft != null)) {
                handLeft.localPosition = Vector3.zero;
                handLeft.SetParent(jointObj.transform);
            }
            else if ((jt == Kinect.JointType.HandRight) && (handRight != null)) {
                handRight.localPosition = Vector3.zero;
                handRight.SetParent(jointObj.transform);
            }
        }

        return body;
    }

    private void RefreshBody(ulong id, GameObject bodyObject) {
        Dictionary<Kinect.JointType, Vector3> joints = bodyTracker3D.GetJoints(id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++) {
            Vector3 sourceJointPosition = joints[jt];
            Vector3? targetJointPosition = null;

            if(KinectJointsMap.Map.ContainsKey(jt)) {
                targetJointPosition = joints[KinectJointsMap.Map[jt]];
            }

//TODO: find more optimised way for this!
            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = sourceJointPosition;

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if(targetJointPosition.HasValue) {
                lr.SetPosition(0, jointObj.position);
//TODO: find solution for local/world coordinates problem!
//=> THIS IS HORRIBLE!
                Transform targetObj = bodyObject.transform.Find(KinectJointsMap.Map[jt].ToString());
                targetObj.localPosition = targetJointPosition.Value;
                lr.SetPosition(1, targetObj.position);
            }
            else {
                lr.enabled = false;
            }
        }
    }

}
