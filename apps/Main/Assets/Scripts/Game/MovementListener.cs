﻿using UnityEngine;
using SimpleJSON;

//TODO: unif messages

public class MovementListener : MonoBehaviour {

    //public MessageSender messageManager;
    public GameController gameController;

//TODO: get "timeInterval" from a GameController or SessionManager object (duplicated data!)
    public Transform head;
    public Transform handLeft;
    public Transform handRight;

    public Transform ball;

    public float timeInterval = .033f; // 30fps

    float delay;


    void Awake() {
        //TODO: check controller
        //...

        if (head == null) {
            throw new System.Exception("ERROR: no transform provided for 'head'!");
        }
        if (handLeft == null) {
            throw new System.Exception("ERROR: no transform provided for 'left hand'!");
        }
        if (handRight == null) {
            throw new System.Exception("ERROR: no transform provided for 'right hand'!");
        }

        if (ball == null) {
            throw new System.Exception("ERROR: no transform provided for 'ball'!");
        }
    }

    void Start() {
        delay = .0f;
    }

    void Update() {
        delay += Time.deltaTime;
        if (delay < timeInterval) {
            return;
        }
        delay = .0f;

        var msg = new JSONClass();

        AddBallInfo(msg, ball.position);

        if (head.hasChanged || handLeft.hasChanged || handRight.hasChanged) {

            AddBodyInfo(msg, 1, head.position, handLeft.position, handRight.position);
            head.hasChanged = false;
            handLeft.hasChanged = false;
            handRight.hasChanged = false;

        }

        //print("sending message: " + msg.ToString());
        //messageManager.SendMove(msg.ToString());
        gameController.NotifyMove(msg.ToString());
    }


    //TODO: move the message stuff to GameController (or even to MessageSender?)
    // (only pass Vector info to controller)
    private void AddBallInfo(JSONClass msg, Vector3 ball) {
        msg["ball"]["x"] = ((int)(ball.x * 100)).ToString();
        msg["ball"]["y"] = ((int)(ball.y * 100)).ToString();
        msg["ball"]["z"] = ((int)(ball.z * 100)).ToString();
    }

    //TODO: move the message stuff to GameController (or even to MessageSender?)
    // (only pass Vector info to controller)
    private void AddBodyInfo(JSONClass msg, int index,
            Vector3 head, Vector3 handLeft, Vector3 handRight) {

        string strIndex = index.ToString();
        msg[strIndex]["position"][0]["x"] = ((int)(head.x * 100)).ToString();
        msg[strIndex]["position"][0]["y"] = ((int)(head.y * 100)).ToString();
        msg[strIndex]["position"][0]["z"] = ((int)(head.z * 100)).ToString();

        msg[strIndex]["position"][1]["x"] = ((int)(handLeft.x * 100)).ToString();
        msg[strIndex]["position"][1]["y"] = ((int)(handLeft.y * 100)).ToString();
        msg[strIndex]["position"][1]["z"] = ((int)(handLeft.z * 100)).ToString();

        msg[strIndex]["position"][2]["x"] = ((int)(handRight.x * 100)).ToString();
        msg[strIndex]["position"][2]["y"] = ((int)(handRight.y * 100)).ToString();
        msg[strIndex]["position"][2]["z"] = ((int)(handRight.z * 100)).ToString();
    }

}
