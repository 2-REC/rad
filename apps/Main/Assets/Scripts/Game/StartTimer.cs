﻿using UnityEngine;
using UnityEngine.UI;

public class StartTimer : MonoBehaviour {

    public Text timerText;
    public Transform panel;

    float timeLeft;


    void Update() {
        timeLeft -= Time.deltaTime;
        timerText.text = timeLeft.ToString("0");
        if (timeLeft < 0) {
            //enabled = false;
            //Destroy(this);
            //panel.enabled = false;
            //enabled = false;
            //Destroy(panel);
            Destroy(this.gameObject);
        }
    }


    public void Begin(float time) {
        timeLeft = time;
        enabled = true;
    }

}
