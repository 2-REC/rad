﻿using UnityEngine;
using SimpleJSON;

//TODO: build messages in MessageSender!

public abstract class IGameController : MonoBehaviour {

    //TODO: make generic?
    //protected CommController commController;
    protected CommControllerAdmin commController;
    protected MessageSender messageSender;


    //TODO: can be in Awake if force script order (CommController before GameController)
    protected void Start() {
        // Find CommController
        GameObject[] commControllers = GameObject.FindGameObjectsWithTag("CommController");
        if (commControllers.Length != 1) {
            throw new System.Exception("ERROR: Problem finding the 'CommController' object!");
        }

        //commController = commControllers[0].GetComponent<CommController>();
        commController = commControllers[0].GetComponent<CommControllerAdmin>();
        if (commController == null) {
            throw new System.Exception("ERROR: Problem finding the 'CommController' component!");
        }
        print("comm controller ok");

        // Find MessageSender from CommController
        messageSender = commControllers[0].GetComponent<MessageSender>();
        if (messageSender == null) {
            throw new System.Exception("ERROR: Problem finding the 'MessageSender' component!");
        }
        print("message sender ok");

    }

    //TODO: could bypass methods 'NotifyStart', etc.
    private void SendGameMessage(string tag, string message) {
        var msg = new JSONClass();
        //TODO: other format for data?
        msg[tag] = message;
        messageSender.SendGame(msg.ToString());
    }

    public void NotifyStart(string message) {
        //TODO: send START message
        print("send START:" + message);

//        messageSender.SendGame("START:" + message);
        SendGameMessage("START", message);
    }

    public void NotifyStop(string message) {
        //TODO: send STOP message
        print("send STOP: " + message);

//        messageSender.SendGame("STOP:" + message);
        SendGameMessage("STOP", message);
    }

    public void NotifyWait(string message) {
        //TODO: send WAIT message
        print("send WAIT: " + message);

//        messageSender.SendGame("WAIT:" + message);
        SendGameMessage("WAIT", message);
    }

    public void NotifyReady(string message) {
        //TODO: send READY message
        print("send READY: " + message);

//        messageSender.SendGame("READY:" + message);
        SendGameMessage("READY", message);
    }

}
