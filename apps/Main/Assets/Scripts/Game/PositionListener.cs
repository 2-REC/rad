﻿using UnityEngine;
using SimpleJSON;

public class PositionListener : MonoBehaviour {

    //public MessageSender messageManager;
    public GameController gameController;

//TODO: get "singlePlayer" from a GameController or SessionManager object (duplicated data!)
// (idem for "timeInterval")
    public bool singlePlayer;
    public Transform position1;
    public Transform position2;

    public float timeInterval = .033f; // 30fps

    float delay;


    void Awake() {
        if (position1 == null) {
            throw new System.Exception("ERROR: no transform1 provided!");
        }
        if (!singlePlayer && (position2 == null)) {
            throw new System.Exception("ERROR: no transform2 provided!");
        }
    }

    void Start() {
        delay = .0f;
    }

    void Update() {
        delay += Time.deltaTime;
        if (delay < timeInterval) {
            return;
        }
        delay = .0f;


        if (position1.hasChanged || (!singlePlayer && position2.hasChanged)) {
            var msg = new JSONClass();

            if (position1.hasChanged) {
                SendBodyInfo(msg, 1, position1.position);
                position1.hasChanged = false;
            }

            if (!singlePlayer && position2.hasChanged) {
                SendBodyInfo(msg, 2, position2.position);
                position2.hasChanged = false;
            }

//print("sending message: " + msg.ToString());
            //messageManager.SendMove(msg.ToString());
            gameController.NotifyMove(msg.ToString());
        }
    }

    private void SendBodyInfo(JSONClass msg, int index, Vector3 position) {
        string strIndex = index.ToString();
        msg[strIndex]["position"]["x"] = ((int)(position.x * 100)).ToString();
        msg[strIndex]["position"]["y"] = ((int)(position.y * 100)).ToString();
        msg[strIndex]["position"]["z"] = ((int)(position.z * 100)).ToString();
    }

}
