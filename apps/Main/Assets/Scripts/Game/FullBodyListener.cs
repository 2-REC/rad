﻿using SimpleJSON;
using UnityEngine;

public class FullBodyListener : MonoBehaviour {

    //public MessageSender messageManager;
    public GameController gameController;
    public float timeInterval = .033f; // 30fps
//    public BodyView bodyView;
    public BodyTrackerView bodyView;

    float delay;


    void Start() {
        delay = .0f;
    }

    void Update() {
        delay += Time.deltaTime;
        if (delay < timeInterval) {
            return;
        }
        delay = .0f;

//TODO: Make a loop on "bodyView.nbTrackedBodies"
        ulong trackedId1 = bodyView.TrackedId1;
        ulong trackedId2 = bodyView.TrackedId2;
        if ((trackedId1 != 0) || (trackedId2 != 0)) {

            var msg = new JSONClass();

            if (trackedId1 != 0) {
                //Debug.Log("Head1 is tracked");
                SendBodyInfo(msg, trackedId1);
            }

            if (trackedId2 != 0) {
                //Debug.Log("Head2 is tracked");
                SendBodyInfo(msg, trackedId2);
            }

            //Debug.log("sending message: " + msg.ToString());
            //messageManager.SendMove(msg.ToString());
            gameController.NotifyMove(msg.ToString());
        }
    }


    void SendBodyInfo(JSONClass msg, ulong trackedId) {
        GameObject body = bodyView.GetBody(trackedId);
        if (body == null) {
            Debug.Log("ERROR: NULL BODY!");
            return;
        }

        int i = 0;
        foreach (Transform child in body.transform) {
            //Debug.Log("child: " + child.name);
            //Debug.Log(" pos: " + child.position);
            msg["1"][i.ToString()]["x"] = ((int)(child.position.x * 100)).ToString();
            msg["1"][i.ToString()]["y"] = ((int)(child.position.y * 100)).ToString();
            msg["1"][i.ToString()]["z"] = ((int)(child.position.z * 100)).ToString();
            ++i;
        }
    }

}
