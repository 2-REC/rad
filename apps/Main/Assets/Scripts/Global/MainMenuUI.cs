﻿using UnityEngine;

public class MainMenuUI : MonoBehaviour {

    public Menu ctrl;

    public void NewGame() {
        ctrl.NewGame();
    }

    public void Options() {
        ctrl.Options();
    }

    public void Credits() {
        ctrl.Credits();
    }

    public void Quit() {
        ctrl.End();
    }

}
