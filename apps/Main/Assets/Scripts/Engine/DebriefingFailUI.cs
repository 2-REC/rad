﻿using UnityEngine;
using UnityEngine.UI;

public class DebriefingFailUI : MonoBehaviour {

    public DebriefingFail debriefingFail;
//    public Text textNbLives;
//    public Text textContinue;
    public Text textPointsResults;
    public Text textTimeResults;


    void Start() {
/*
        textNbLives.text = "Lives left: " + debriefingFail.GetGameData().GetLives().ToString();

        int nbLives = debriefingFail.GetGameData().GetLives();
        textNbLives.text = "Lives left: " + nbLives.ToString();
        if (nbLives > 0) {
            textContinue.text = "Have a life => RETRY";
        }
        else {
            textContinue.text = "Don't have lives => GAME OVER";
        }
*/
        textPointsResults.text = ((GameData)debriefingFail.GetGameData()).GetPoints().ToString();
        textTimeResults.text = ((GameData)debriefingFail.GetGameData()).GetTimeLeft().ToString("F2");
    }

    public void Continue() {
        debriefingFail.Continue();
    }

}
