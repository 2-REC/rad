﻿/*
 Example implementation of "Level" with:
 - implementation of "InitGame"
 - calls to "EndLevel"
 - basic HUD management
 - basic level controller management
 - 2 buttons to simulate success or failure of level (TO BE REMOVED!)

 Remark: The links with the Hud controller & level controller can be done
  differently depending on the scene's strcture/hierarchy.
*/

using System.Collections;
using UnityEngine;

//TODO: change this, horrible...
delegate void LevelManagerUpdateProcess();

public class LevelManager : Level {

    //////// GAME - BEGIN
    int health;
    int points;
//////// RAD_TIMER - MID
/*
    //TODO: get from config
    public float gameLength = 60.0f; // 1 minute
    public float startDelay = 2.0f;
    public float timeStep = 1.0f;
*/
//TODO: remove "timeStep" stuff => makes time slower!
    float timeStep;
    LevelManagerUpdateProcess process;
    float timeLeft;
    float nextValue;
//////// RAD_TIMER - END
    //////// GAME - END

    public float flashSpeed = 5.0f;
    public Color flashColour = new Color(1.0f, 0.0f, 0.0f, 0.1f);


    LevelController levelController;
    HudController hudController;

    float timer;
    float startDelay = 0.0f;
    float delay = 0.0f;

//TODO: public? (as property with private "set"?)
    public bool doingSetup = true;



    //////// GAME - BEGIN
//////// RAD_TIMER - MID
    private void Update() {
        process();
    }

    private void WaitInit() {
        ;
    }

    private void UpdateTimer() {
        timeLeft -= Time.deltaTime;
/*
        if (timeLeft < nextValue) {
            hudController.SetTimer(nextValue);
            nextValue -= timeStep;
            if (timeLeft < .0f) {
                timeLeft = .0f;
                NotifyWin();
                process = new LevelManagerUpdateProcess(WaitInit);
            }
        }
*/
        if (timeLeft < .0f) {
            timeLeft = .0f;
            NotifyWin();
            process = new LevelManagerUpdateProcess(WaitInit);
        }
        hudController.SetTimer(timeLeft);
    }
    //////// RAD_TIMER - END
    //////// GAME - END



    protected override void InitGame() {
        doingSetup = true;

        //////// GAME - BEGIN
        GameData gameData = (GameData)GetGameData(); 
        health = gameData.GetHealth();
        points = gameData.GetPoints();
        //////// GAME - END

        levelController = (LevelController)FindObjectOfType(typeof(LevelController));
        hudController = levelController.getHudController();

        //////// GAME - BEGIN
        hudController.SetLives(lives);
        hudController.SetPoints(points);
        hudController.InitLifebar(health, gameData.GetInitialHealth());
//////// RAD_TIMER - MID
        process = new LevelManagerUpdateProcess(WaitInit);
/*
        timeLeft = gameLength;
        nextValue = gameLength - timeStep;
        hudController.SetTimer(timeLeft);
*/
//TODO: CHANGE THAT!!!!
        timeLeft = levelController.GetGameLength();
        timeStep = levelController.GetTimeStep();
        nextValue = timeLeft - timeStep;
        hudController.SetTimer(timeLeft);
//////// RAD_TIMER - END
        //////// GAME - END

        startDelay = levelController.InitLevel();
        Invoke("StartLevel", startDelay);
    }

    protected override void EndProcess() {

        //////// GAME - BEGIN
        // Update Game Data that should be saved when finishing the level (win or fail)
        //...
        //((GameData)GetGameData()).SetPoints(points);
//////// RAD_TIMER - MID
        //TODO: get from config
        if (timeLeft < .0f) {
            timeLeft = .0f;
        }
        ((GameData)GetGameData()).SetTimeLeft(timeLeft);
//////// RAD_TIMER - END
        //////// GAME - END
    }

    protected override void SaveProcess() {
        //////// GAME - BEGIN
        // Update Game Data that should be saved when saving the level
        //...
        GameData gameData = (GameData)GetGameData(); 
        gameData.SetHealth(health);
        gameData.SetPoints(points);
        //////// GAME - END
    }


    void StartLevel() {
        print("start level");
        doingSetup = false;

//////// RAD_TIMER - MID
        if (!doingSetup) {
            process = new LevelManagerUpdateProcess(UpdateTimer);
        }
//////// RAD_TIMER - END

        levelController.StartLevel();
    }

    IEnumerator Completed() {
        timer = 0;
        while (timer < delay) {
            timer += Time.deltaTime;
            if (timer >= delay) {
                EndLevel(true);
            }
            yield return null;
        }
    }

    IEnumerator Failed() {
        timer = 0;
        while (timer < delay) {
            timer += Time.deltaTime;
            if (timer >= delay) {
                EndLevel(false);
            }
            yield return null;
        }
    }


    public void NotifyWin() {
        levelController.StopLevel();
        delay = levelController.NotifyWin();

        if (delay == 0.0f) {
            delay = 0.0000001f; // horrible hack!
        }
        StartCoroutine("Completed");
    }

    public void NotifyDeath() {
        //////// GAME - BEGIN
        hudController.SetLives(lives - 1);
        //////// GAME - END

        levelController.StopLevel();
        delay = levelController.NotifyLose();

        if (delay == 0.0f) {
            delay = 0.0000001f; // horrible hack!
        }
        StartCoroutine("Failed");
    }

    //////// GAME - BEGIN
/*
    public void NotifyHit(int damage) {
        health -= damage;
        if (health < 0) {
            health = 0;
        }

        hudController.Flash(flashColour, flashSpeed);
        hudController.SetLifebar(health);

        if (health == 0) {
            NotifyDeath();
        }
    }
*/
    public bool NotifyHit(int damage) {
        health -= damage;
        if (health < 0) {
            health = 0;
        }

        hudController.Flash(flashColour, flashSpeed);
        hudController.SetLifebar(health);

        if (health == 0) {
            NotifyDeath();
        }
        return (health == 0);
    }
    //////// GAME - END

//TODO: Can add bonus stuff ...
/*
    public void NotifyHeal(int life) {
    }
*/

    //////// GAME - BEGIN
    public void AddPoints(int amount) {
        points += amount;
        hudController.SetPoints(points);
    }
    //////// GAME - END

    //////// GAME - BEGIN
//TODO: Keep? Here? Do same for other fields?
    public int GetHealth() {
        return health;
    }
    //////// GAME - END

    public override void Pause() {
//?        hudController.Pause();
//?        levelController.Pause();
    }

    public override void Unpause() {
//?        hudController.Unpause();
//?        levelController.Unpause();
    }

    public void Quit() {
//TODO: OK, anything else to do?
// (eg: stop controller? Free stuff? ...?)
        QuitGame();
    }

}
