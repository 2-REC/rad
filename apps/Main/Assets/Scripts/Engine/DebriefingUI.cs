﻿using UnityEngine;
using UnityEngine.UI;

public class DebriefingUI : MonoBehaviour {

    public Debriefing debriefing;
    public Text textPointsResults;


    void Start() {
        textPointsResults.text = ((GameData)debriefing.GetGameData()).GetPoints().ToString();
    }

    public void Continue() {
        debriefing.Continue();
    }

}
