﻿/*
  This script can be used to test/simulate the "Map" screen.
  It needs to be attached to a UI Canvas, and the "Map" script has to be provided as input parameter.
  /// (+other params...)
*/

using UnityEngine;
using UnityEngine.UI;

public class MapTestsUI : MonoBehaviour {

    public Map ctrl;
    public Text text;
    public GameObject panelMap;
    public GameObject buttonLevelPrefab;


    void Start() {
        //TODO: change that
        //text.text = ctrl.StateId.ToString();

        float positionY = 50.0f;
        foreach(int level in ctrl.GetLevels()) {
            GameObject button = (GameObject)Instantiate(buttonLevelPrefab);
            button.transform.SetParent(panelMap.transform);
            button.GetComponent<Button>().onClick.AddListener(delegate{ctrl.StartLevel(level);});
            //// RAD - BEGIN
            ////TODO: change that
            //button.transform.GetChild(0).GetComponent<Text>().text = "Level" + level;
            //// RAD - MID
            print("level " + level);
            string levelName = ctrl.GetGameData().GetLevelName(level);
            button.transform.GetChild(0).GetComponent<Text>().text = levelName;
            //// RAD - END
            button.transform.localPosition = new Vector3(0.0f, positionY, 0.0f);
            positionY -= 50.0f;

/*
            // highlight if completed
            if (ctrl.GetGameData().IsLevelCompleted(level)) {
//TODO: change colour or size...
            }
*/
        }
    }

}
