# APPS

## PROJECTS

### MAIN

<b>TODO!</b>

The main application, the server...<br>
...<br>


2 modes, with or without OpenCV, analysing the Kinect video and depth streams to track colour balls.
...<br>


### PLAYERS

<b>TODO!</b>

The client application, in 2 versions
* RAD Player
* RAD Player Gyro: combining data sent by the Kinect to realign the view (to avoid "drifting" away)
...<br>


Additionally, 2 other "player" applications are provided.
They both show how 2 players can be handled simultaneously, able to move around the game area.

* Client: Players are represented by a head and a cylindrical body, the position of their head being tracked.
* Client2: Players are represented by a "stickman" figure, their full body being tracked.

These 2 applications are deprecated, and are mostly kept as reference.


### EXTRAS

TODO: "Spacebrew Admin"
=> To monitor connections in real-time, allowing to check if the server is running and the connections with clients are set properly.


## COMPONENTS

### DEVICES

* Computer
    * Windows 10
    * Unity 2019.1 or latest
    * Vuforia (SDK?) package installed in Unity
    <br>TODO:
    * other Unity packages? (XR? VR? ...?)


* Microsoft Kinect v2
    * SDK 2.0 installed on main computer


* Android phone
    * rear camera
    * gyroscope/accelerometers
    <br>TODO:
    * latest version?
    * required libs/apps?
    * ...


* Wi-Fi network
    * All the devices have to be connected to the same local network.


### MODULES

The project uses the several libraries/modules.


#### UNITY-MULTI

The [unity-multi](https://github.com/2-REC/unity-multi) project provides network communications in Unity.<br>
It is based on the [Spacebrew Unity](https://github.com/Spacebrew/spacebrewUnity) project and uses the [Spacebrew](http://docs.spacebrew.cc/) toolkit.<br>


#### UNITY-KINECT-TRACKING

The [unity-kinect-tracking](https://github.com/2-REC/unity-kinect-tracking) project provides human body tracking in Unity by combining the Microsoft Kinect device with the OpenCV image processing library.<br>
It combines the 2 projects:

* <b>unity-kinect-boilerplate</b>: Providing tracking features in Unity using the [MS Kinect v2](https://developer.microsoft.com/en-us/windows/kinect/).
* <b>unity-opencv-boilerplate</b>: Providing image processing features in Unity using the [OpenCV](https://opencv.org/) library.


#### UNITY-STATE-ENGINE

The [unity-state-engine](https://github.com/2-REC/unity-state-engine) project provides state machine management in Unity.<br>
The project allows to define application states (divided in a "Global" and a "Game" graphs), and automatically manage the transitions between each state.


## SETUP

### DEPENDENCIES

First, make sure all the required modules and libraries are available.


#### UNITY-MULTI

The package is provided with the project, and is located in the "apps/Libraries/unity-multi" directory.
It is provided as a package and a ZIP file, required by both the "Main" and "Players" applications (hence why it is not included directly in the Unity projects, to avoid duplicates).

If desired, the latest version can be found at the [unity-multi GitHub repository](https://github.com/2-REC/unity-multi).

<b>NOTE:</b> The compatibility with newer versions isn't fully guaranteed.


#### UNITY-KINECT-TRACKING

The package is provided with the project, already included in the Unity project (in the "Assets/UnityKinectTracking" folder).

If desired, the latest version can be found at the [unity-kinect-tracking GitHub repository](https://github.com/2-REC/unity-kinect-tracking).

<b>NOTE:</b> The compatibility with newer versions isn't fully guaranteed.


The module uses additional libraries:

* [OpenCV](https://opencv.org/)
    <br>Pre-compiled libraries (Debug & Release) are provided with the project (located in "OpenCV/Plugins").

* [Kinect for Unity package](https://go.microsoft.com/fwlink/p/?LinkId=513177)
    <br>The Unity package is provided with the project (located in "apps/Libraries").

* [MS Kinect SDK 2.0](https://developer.microsoft.com/en-us/windows/kinect/)
    <br>The SDK must be downloaded and installed on the host machine to which the Kinect will be connected (where the "Main" application will be running).

<b>NOTE:</b> Again, newer versions might be available for download, however the compatibility isn't fully guaranteed.


#### UNITY-STATE-ENGINE

The package is provided with the project, already included in the Unity project (in the "Assets/StateEngine" folder).

If desired, the latest version can be found at the [unity-state-engine GitHub repository](https://github.com/2-REC/unity-state-engine).

<b>NOTE:</b> The compatibility with newer versions isn't fully guaranteed.


### INSTALLATION

* Clone the Git repository

    ```
    git clone https://gitlab.com/rmit-xr-lab/rad-project/rad
    ```

    Or download and extract the [ZIP archive](https://gitlab.com/rmit-xr-lab/rad-project/rad/-/archive/master/rad-master.zip)


* Set the dependencies

    * <b>unity-multi</b>
        > This module is required by both the "Main" and "Players" projects.

        * Extract the "StreamingAssets" archive<br>
            The content of the extracted directory should be copied to the "Assets/StreamingAssets" directory of the Unity project.
            <b>NOTE:</b> The "Examples" files are not required so don't have to be imported into the project.
        * Import the package in Unity.<br>
            The imported assets should be located in the "Assets/Spacebrew" folder of the Unity project.

    * <b>unity-kinect-opencv</b>
        > This module is only required by the "Main" project.

        * The "UnityKinectTracking" is provided with the Unity project.<br>
            <b>NOTE:</b> In case the module was downloaded separately, it should be extracted in the "Assets/UnityKinectTracking" directory of the Unity project. Additionally, the OpenCV libraries must be present in the underlying "OpenCV/Plugins".

    * <b>Kinect 2.0 Unity package</b>
        > This module is only required by the "Main" project.

        The Unity package is provided with the project (located in "apps/Libraries").
        The package should be imported in Unity, and its content moved to a "Kinect" folder inside "Assets".<br>
        <b>NOTE:</b> See "KINECT" section below for details.

    * <b>unity-state-engine</b>
        > This module is only required by the "Main" project.

        * The "StateEngine" is provided with the Unity project.<br>
            <b>NOTE:</b> In case the module was downloaded separately, it should be extracted in the "Assets/StateEngine" directory of the Unity project.


#### ALTERNATIVE

In order to save space, or to avoid having to maintain more than 1 version of the "unity-multi" module, it is possible to share a single copy between the different projects (the installation can be quite big, as a version of Node.js is included, with a potentially big number of node modules).

Instead of copying the "unity-multi" module in each project:

* Extract the "StreamingAssets" archive in "Libraries/StreamingAssets".
* Run <b>set_links.bat</b> script as Administrator.
    > It will create the symbolic links between the projects and the submodule, avoiding the need to duplicate the assets for each project using them.

    <b>NOTE:</b> The script has to be run from a CMD window (seems like it doesn't work if right-click + "Run As Administrator")


#### KINECT

The "Kinect for Unity" package allows to use the Kinect in Unity.

The provided Unity package needs to be imported in the project.<br>
If preferred, or if the latest version of the package is desired, it can be downloaded from the Microsoft website.<br>
However, it doesn't seem to change anymore, so the provided package should be enough.

* <i>OPTIONAL:</i> Download the [Kinect for Unity package](https://go.microsoft.com/fwlink/p/?LinkId=513177)
    <br><b>NOTE:</b> It is mentioned that it is a package for Unity Pro, but it works fine with the free Unity version.<br>


* Import the "Kinect for Unity" package
    * "Assets" -> "Import Package" -> "Custom Package..."
    * Navigate to "apps/Libraries" or to the download location if the package was downloaded separately (previous step).
    * Select the package:
        <br>"Kinect.2.0.1410.19000.unitypackage" (or different version if downloaded separately)
    * Import the full package:
        <br>=> Both "Plugins" and "Standard Assets"


* Move the imported assets folders to the "Kinect" folder
    * "Plugins"
    * "Standard Assets"
    <br><b>NOTE:</b> MAKE SURE TO ONLY MOVE THE ASSETS RELATED TO KINECT! (the ones imported previously)


The last step is not required, but helps keeping a cleaner assets folder.<br>
(Also it is required to be like this in order for the ".gitignore" to work properly when using Git)<br>




## USAGE

<b>TODO!</b>

CONFIGURATION

To connect to the server, the application reads the connection parameters (server's address and port) in an external text file.<br>
This is to allow a manual configuration before starting the application, as there is currently no way to specify these parameters manually once in the application (there is no UI).<br>

If the file doesn't exist (typically at the application's first execurtion), it is created with the default values (these can be changed in the Unity project).<br>
The application should then be closed, and the file edited to set the desired values.<br>

The location of the file depends on the platform:<br>
* Windows:<br>
    C:\Users\...\AppData\LocalLow\rmit\rad
* Android:<br>
    /storage/emulated/0/android/data/com.rmit.rad/files


## DISCLAIMER

Thanks to the creators of the following tools and projects:

* [Spacebrew](http://docs.spacebrew.cc/): Network related features
* [Spacebrew Unity](https://github.com/Spacebrew/spacebrewUnity): Integration of Spacebrew in Unity
* [OpenCV](https://opencv.org/): Image processing features
* [Unity-SimpleGrid-Shader](https://github.com/r3eckon/Unity-SimpleGrid-Shader) from [r3eckon](https://github.com/r3eckon): Grid shader in Unity ([more info](https://forum.unity.com/threads/simple-grid-shader-open-source-gui-controlled-procedural-grid-shader-material.534353/))
* [Microsoft Kinect](https://developer.microsoft.com/en-us/windows/kinect/): Body tracking, and integration in Unity
* [Vuforia](https://developer.vuforia.com/): AR features ("Players" applications)
* [Unity](https://unity.com/): The entire project
