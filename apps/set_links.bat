@echo off

IF exist Main (
    pushd .

    cd Main
    cd Assets

    IF not exist StreamingAssets (
        md StreamingAssets
    )
    cd StreamingAssets
    mklink /D .node "..\..\..\Libraries\unity-multi\StreamingAssets\.node
    mklink /D .spacebrew "..\..\..\Libraries\unity-multi\StreamingAssets\.spacebrew
    cd..

    popd

) ELSE (
    echo 'Main' doesn't exist!
)


IF exist Players (

    cd Players

    rem MAKE A LOOP INSTEAD OF REPEATING FOR EACH PROJECT

    IF exist RAD_Player (
        pushd .

        cd RAD_Player
        cd Assets

        IF not exist StreamingAssets (
            md StreamingAssets
        )
        cd StreamingAssets
        mklink /D .node "..\..\..\..\Libraries\unity-multi\StreamingAssets\.node
        mklink /D .spacebrew "..\..\..\..\Libraries\unity-multi\StreamingAssets\.spacebrew
        cd..

        popd

    ) ELSE (
        echo 'RAD_Player' doesn't exist!
    )


    IF exist Client (
        pushd .
        
        cd Client
        cd Assets
        
        IF not exist StreamingAssets (
            md StreamingAssets
        )
        cd StreamingAssets
        mklink /D .node "..\..\..\..\Libraries\unity-multi\Assets\StreamingAssets\.node
        mklink /D .spacebrew "..\..\..\..\Libraries\unity-multi\Assets\StreamingAssets\.spacebrew
        cd..
        
        md SpaceBrew
        cd SpaceBrew
        mklink /D Editor "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Editor
        mklink /D NodeJs "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\NodeJs
        mklink /D Plugins "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Plugins
        mklink /D Prefabs "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Prefabs
        mklink /D Scripts "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Scripts
        cd ..
        
        popd

    ) ELSE (
        echo 'Client' doesn't exist!
    )


    IF exist Client2 (
        pushd .
        
        cd Client2
        cd Assets
        
        IF not exist StreamingAssets (
            md StreamingAssets
        )
        cd StreamingAssets
        mklink /D .node "..\..\..\..\Libraries\unity-multi\Assets\StreamingAssets\.node
        mklink /D .spacebrew "..\..\..\..\Libraries\unity-multi\Assets\StreamingAssets\.spacebrew
        cd..
        
        md SpaceBrew
        cd SpaceBrew
        mklink /D Editor "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Editor
        mklink /D NodeJs "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\NodeJs
        mklink /D Plugins "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Plugins
        mklink /D Prefabs "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Prefabs
        mklink /D Scripts "..\..\..\..\Libraries\unity-multi\Assets\SpaceBrew\Scripts
        cd ..
        
        popd

    ) ELSE (
        echo 'Client2' doesn't exist!
    )


) ELSE (
    echo 'Players' doesn't exist!
)
