# RAD

## INTRODUCTION
Research project to build an Extended Reality (VR/AR) game without a VR headset.

The project simulates the VR features by tracking the player's body movements using a Kinect, and transmitting the information to the player's mobile device (running Android).
The application on the device then uses this information to position the player in the game world, simulating real-time position tracking.

Additionally, the player's device uses its internal gyroscope and accelerometers to determine the device's rotational movements, thus adding orientation tracking.


As opposed to traditional VR tracking, the system tracks the entire body, allowing a wider range of possibilities for gameplay.


The implemented game here is a remake/variant of the classic retro game PONG, bringing it to the 3rd dimension in Extended Reality.
The player needs to touch the bouncing ball with one of its paddles controlled by hands, in order to push the ball back and avoid it touching the wall behind the player.

The game can easily be modified to add a second player.


### BONUS
TODO: Rename! + Move to other location/section

Depending on the conditions in which the game is played, the gyroscope can suffer precision problems, and become unreliable. A "drift" can appear as the game plays, which can increase with time, making the game completely unplayable (the player facing a direction but the game directing him in the opposite one).

In order to solve this issue, a solution was imagined to "fix" this offset by realigning the virtual orientation to the real world orientation.

The system works by tracking colour balls attached to the player's headset (2 balls minimum are required, of distinct bright colours). The positions of the balls are tracked by combining image analysis (blob detection and tracking) with depth data provided by the Kinect sensor. Combining the information, the positions of the balls in 3D are determined, as well as their relative position to each other. From there, the orientation of the headset can be determined, and the information is sent to the player's device to inform it of the "real world" orientation.
The system is described in more details in the provided documents.

It is experimental, and far from being perfect. It serves more as a proof-of-concept.


## APPLICATIONS

The "app" directory contains the different application modules for the game:

* Main: The main game, also acting as the server
* Players: Different client applications, to be installed on player's portable device

For more information, look at the README file in the "app" directory.


## DOCUMENTS

The "docs" directory contains documents about the project research and findings.

TODO: Need to update/filter the documents.
